#ifndef NN_TIME_INSPECTOR_H_
#define NN_TIME_INSPECTOR_H_
#pragma once

#include <time.h>
#include <stdio.h>
#include <assert.h>
/*
 * TimeInspector class.
 *
 * MAKE SURE YOU DEFINE "NNLIBS_TIMEINSPECTOR_IMPL_" IN ONE CPP FILE AND ONLY ONE.
 *
 * Minimal time mesurement class.
 * Uses RAII and C standard library to display time mesured in the standard output stream.
 * This means the lifetime of an instance of the TimeInspector class
 * defines the time mesured. It is local to the thread in which it is created.
 * You can call displayImmediate() function at any point to get elapsed time.
 * Future features: customize output stream, precision/unit, access elapsed time through a getter.
 */
namespace nnl
{
/* Class definition */
	class TimeInspector
	{
		public: 
			explicit TimeInspector(char const * identifier = "no_id");

			//will call displayImmediate() and release the identifier ptr
			~TimeInspector() noexcept;

			//display elapsed time w/ immediate clock update
			void displayImmediate() const;

		private:
			const char*  nameIdentifier_;
			const std::clock_t clkStart_;
	};

/* Implementation */
#ifdef NNLIBS_TIMEINSPECTOR_IMPL_
	TimeInspector::TimeInspector(char const * identifier)
		: nameIdentifier_(identifier), clkStart_(std::clock())
	{
		assert(("[TimeInspector] You cannot set the identifier field to nullptr!", nameIdentifier_ != nullptr));
	}

	TimeInspector::~TimeInspector() noexcept
	{
		displayImmediate();
		nameIdentifier_ = nullptr;
	}

	void TimeInspector::displayImmediate() const
	{
		std::printf("[TimeInspector] %s : %f.2 ms\n", nameIdentifier_, 1000.0*(std::clock() - clkStart_) / CLOCKS_PER_SEC);
		return;
	}
#endif // NNLIBS_TIMEINSPECTOR_IMPL_
}

#endif // NN_TIME_INSPECTOR_H_
