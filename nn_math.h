#ifndef NN_VECTORS_H_
#define NN_VECTORS_H_
#pragma once

#include <array>
#include <cmath>

/*
 * General purpose math lib.
 */
namespace nnl
{
    namespace math
    {
    /* Constants */
    const float PI = 3.14159265359;
    const float TAU = 6.28318530718;
    /*
    * Struct-free vector math lib.
    * Based on STL containers with typedef.
    */
    namespace vec
    {
        /* Typedefs */
        typedef std::array<float, 2> fVec2;
        typedef std::array<float, 3> fVec3;
        typedef std::array<float, 4> fVec4;

        /* Functions */
        float length(const fVec2& v);
        float length(const fVec3& v);
        float length(const fVec4& v);

        fVec2 normalize(const fVec2& v);
        fVec3 normalize(const fVec3& v);
        fVec4 normalize(const fVec4& v);

        float dot(const fVec2& v0, const fVec2& v1);
        float dot(const fVec3& v0, const fVec3& v1);
        float dot(const fVec4& v0, const fVec4& v1);

/* Implementation */
#ifdef NNLIBS_VECTOR_IMPL_
        float length(const fVec2& v) { return sqrt(v[0]*v[0] + v[1]*v[1]); }
        float length(const fVec3& v) { return sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]); }
        float length(const fVec4& v) { return sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2] + v[3]*v[3]); }

        fVec2 normalize(const fVec2& v) { const float n = length(v); return {{v[0] / n, v[1] / n}};}
        fVec3 normalize(const fVec3& v) { const float n = length(v); return {{v[0] / n, v[1] / n, v[2] / n}};}
        fVec4 normalize(const fVec4& v) { const float n = length(v); return {{v[0] / n, v[1] / n, v[2] / n, v[3] / n}};}

        float dot(const fVec2& v0, const fVec2& v1) { return v0[0] * v1[0] + v0[1] * v1[1]; }
        float dot(const fVec3& v0, const fVec3& v1) { return v0[0] * v1[0] + v0[1] * v1[1] + v0[2] * v1[2]; }
        float dot(const fVec4& v0, const fVec4& v1) { return v0[0] * v1[0] + v0[1] * v1[1] + v0[2] * v1[2] + v0[3] * v1[3]; }
#endif //NNLIBS_VECTOR_IMPL_
    }  
    }
}

#endif // NN_VECTORS_H_
