# nnano_libs

A set of very small header-only libraries/utilities I wrote for my own use in various projects.  
It is NOT a standard library replacement as it relies on many of its features.  

## Libraries

| Name | Info | File |
|------|------|------|
|TimeInspector| A very simple time mesuring utility class| ```nn_time_inspector.h```|
|Math| A small utility header file for math (only vector types for now) | ```nn_math.h```|

## Stuff I would like to make later

+ A std::thread manager kinda thing (a threadpool basically)
+ I/O for PPM files
+ Some custom allocator experiments
+ Logging
+ Tree datastructures
+ Add stuff to the math "lib", namely FFT (and its inverse), AABB intersection/struct, graph algorithms, etc
+ Basic SIMD wrapper